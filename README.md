# darkalloy-e16

![Latest release](https://gitlab.com/digitalrayne/darkalloy-e16/-/badges/release.svg)

![DarkAlloy theme logo](artwork/startup_logo.png)

This is a repackaging of the DarkAlloy E16 (Enlightenment DR16) theme by DarkAlloy.

This is assembled from the most recent versions of the theme files I could find in various archives, as DarkAlloy no longer seems to have a web presence.

## Purpose

I (Rayne) have been using this theme (and E16) since I was a teenager, and I wanted to preserve and make available packaged versions of this theme. Having the theme in a `git` repository means I can set up CI and release tarballs, which makes it easier to package for usage in different OSes which still package E16.

If you find any issues with this theme feel free to open up an issue.

## Releases

[Releases](https://gitlab.com/digitalrayne/darkalloy-e16/-/releases) track the tags in this repository, and follow semantic versioning.

The main branch is used for development, and I cut a release when there are changes.

GitLab releases are used, 

## Theme Description

This is the DarkAlloy theme for Enlightenment DR-0.16.

It was created, and is maintained, by DarkAlloy.

## Original Links

Preserved for attribution and historical reasons.

DarkAlloy's email: scarab@egyptian.net

Original Homepages:
 - http://www.egyptian.net/~scarab
 - http://sublevel27.com/cricketwings
 - http://darkalloy.structum.com.mx
